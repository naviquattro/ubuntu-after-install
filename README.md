# Things to Do After Installing Ubuntu (current 18.04)

This is a WIP list of thing I repeat after each installation of Ubuntu (mostly console stuff).

## Settings

Thanks to OMG!Ubuntu this is an easy command to allow minimizing a window by clicking on its icon.

```sh
$ gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize'
```

## Aliases

Shortcut for updating the system - update + upgrade + cleanup.

```sh
$ alias up='sudo apt update && sudo apt upgrade -y && sudo apt autoremove -y'
```

Git shortcuts
```sh
$ git config --global alias.co checkout
$ git config --global alias.br branch
$ git config --global alias.ci commit
$ git config --global alias.st status
$ git config --global alias.di diff
$ git config --global alias.new 'checkout -b'
```

Check the weather. By default it shows weather for a location where the IP points to, or you can pass a location name as a parameter.

```sh
$ weather() {
  curl wttr.in/$1
}
```

## Software

* Midnight Commander `$ sudo apt install mc`
* curl `$ sudo apt install curl`
* [htop](https://github.com/hishamhm/htop) system monitor `$ sudo apt install htop`
* Git and text mode [interface](https://jonas.github.io/tig/) for it `$ sudo apt install git tig`

## Java
The Sun Java is needed for Eclipse and other stuff.
Download:
```sh
wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/9.0.4+11/c2514751926b4512b076cc82f959763f/jdk-9.0.4_linux-x64_bin.tar.gz
```

Prepare directories and unpack the tarball:
```sh
$ sudo mkdir -p /usr/lib/jvm
$ sudo tar -C /usr/lib/jvm -xf jdk-9.0.4_linux-x64_bin.tar.gz
```

Configure:
```sh
$ sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.9.0/bin/java" 1
$ sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.9.0/bin/javac" 1
$ sudo update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/lib/jvm/jdk1.9.0/bin/javaws" 1
$ sudo chmod a+x /usr/bin/java
$ sudo chmod a+x /usr/bin/javac
$ sudo chmod a+x /usr/bin/javaws
$ sudo chown -R root:root /usr/lib/jvm/jdk1.9.0
```

## All togather

One command to apply the above to the system.

```sh
$ echo $'alias up=\'\n# Update the system\nsudo apt update && sudo apt upgrade -y && sudo apt autoremove -y\'\n\n# Weather\nweather() {\n  curl wttr.in/$1\n}' >> ~/.bashrc && sudo apt install -y mc htop git tig
```
